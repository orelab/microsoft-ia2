
# Lundi LevelUp Modélisation !

- principe tables - relations
- clés primaires - clés étrangères
- dictionnaire des données - MCD - MLD


## Hotel IBIS

Vous gérez une chaîne d'hotels IBIS, et devez modéliser une 
base de données permettant d'enregistrer les réservations de 
vos clients. Dans l'optique des les afficehr sur une carte,
vous devrez enregistrer les adresses, coordonnées géographiques,
type d'hotel (Ibis, Budget, Styles).

Chaque hôtel coprend une liste de chambres. Chacune a un type
(simple, double, suite, etc...) et un prix à la nuité.

Vous devez évidemment enreigstrer les informations des clients,
les montants réglés, date de début, de fin de séjour, etc...


## E-commerce

Vous avez inventé et breveté des stylos comestibles, qui font
actuellement fureur sur le marché des goodies :D et souhaitez
maintenant les vendre en direct sur la toile. Créer un modèle
permettant de créer un catalogue de produits simples (prévoyez
tout-de-même que ces produits aient comme options : couleurs,
goût), uen gestion simple des stocks, la possibilité d'enregistrer
les infos des clients, ainsi que leurs commandes !


## Marcheurs

Sur le chemin de Saint-Jacques de Compostelle, beaucoup de pèlerins
sont âgés, et éprouvent des difficultés à transporter leurs bagages.

Pour les aider, vous avez lancé une activité de transport des bagages
par fourgonnettes. Comme vos clients ont besoin de leurs bagages 
chaque soir, vous devez prévoir d'enregistrer les adresses de chaques 
étapes de vos clients ! Notez qu'un trajet est constitué d'un point
de départ, un point d'arrivé, et est nécessairement réalisé sur un 
jour et un seul (les pélerins ne marchent pas la nuit !).


## TODO

Pour chacun des trois sujets, créer le MCD, le MLD, et la
base de données sous MariaDB !




# Mardi LevelUp SQL !

- requêtes de création de table, contraintes d'intégrité
- requêtes de sélection, ajout, modification et suppression
- Bonus : les index





# Outils

- https://launchpad.net/analysesi
- https://www.mocodo.net/
- https://hub.docker.com/r/phpmyadmin/phpmyadmin/


## MariaDB

- cd mariadb
- docker-compose up

## MongoDB

- docker run --name voyageur -d mongo:latest






## TODO

- refaire brief IA             OK
- GIT audrey                   OK
- point mail wetransfer        
- mail Labège#5                OK
- point Azure


