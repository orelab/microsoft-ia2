
# Édition du modèle

Créez la base de données dont le schéma vous est fourni, avec des requêtes SQL.
Vous pouvez vous aider d'un générateur de scripts SQL !


# Requêtes de sélection

- afficher la liste des clients
- afficher la liste des 10 derniers clients
- afficher la liste des camions, triés par ordre de plaque d'immatriculation
- compter le nombre de clients
- compter le nombre de clients de l'année en cours
- compter le nombre de clients de l'année dernière
- compter le nombre de bagages transportés à une date donnée
- afficher la liste des chauffeurs, avec pour chacun le nombre de tournées qu'il a 
  effectuées
- afficher la liste des clients, ordonnée par nombre de bagages transportés
- afficher les 10 gîtes qui ont le plus de succès
- afficher le nombre moyen de bagages transpotés pour un client 
- afficher le camion qui a réalisé le plus de tournées
- afficher le camion qui a le plus visité le gîte n°10
- afficher un planning chauffeur pour un jour donné (liste des gites dans l'ordre
  de leur index, puis la liste des bagages à récupérer, ainsi que la liste des
  bagages à déposer)


# Requêtes d'insertion

- ajoutez un nouveau client
- ajoutez trois étapes pour ce client, repectivement pour demain, après-demain et le 
  jour suivant
- ajoutez une tournée pour demain, dont le chauffeur et le camion sont respectivement
  les derniers de la liste


# Requêtes de mise à jour

- mettez à jour toutes les étapes de demain afin qu'elles soient reliées à la tournée 
  du même jour précédemment créée.


# Requêtes de suppression

- supprimez les clients qui n'ont jamais rien acheté
- supprimez les gîtes qui n'ont jamais fait l'objet, ni d'un départ, ni d'une arrivée
- supprimez les étapes qui n'ont soit pas de gite de départ, soit pas de gîte d'arrivée
- supprimez les chauffeurs dont le nom ou le prénom contient le mot "TEST" en majuscules
