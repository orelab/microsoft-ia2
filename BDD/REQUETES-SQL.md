# afficher la liste des clients

SELECT * FROM client;

# afficher la liste des 10 derniers clients

SELECT * FROM client LIMIT 10;

# afficher la liste des camions, triés par ordre de plaque d'immatriculation

SELECT * FROM camion ORDER BY immatriculation_camion;

# compter le nombre de clients

SELECT COUNT(*) FROM client;

# compter le nombre de clients de l'année en cours

SELECT COUNT(*) from (
	SELECT id_client, prenom_client, nom_client
	FROM client 
	JOIN etape ON client.id_client = etape.fk_client
	WHERE YEAR(date_etape) = YEAR(CURRENT_DATE)
	GROUP BY id_client
) AS liste_clients;

ou

SELECT COUNT(DISTINCT(id_client))
FROM client 
JOIN etape ON client.id_client = etape.fk_client
WHERE YEAR(date_etape) = YEAR(CURRENT_DATE);

# compter le nombre de clients de l'année dernière

SELECT COUNT(DISTINCT(id_client))
FROM client 
JOIN etape ON client.id_client = etape.fk_client
WHERE YEAR(date_etape) = YEAR(CURRENT_DATE) - 1;

# compter le nombre de bagages transportés à une date donnée

SELECT COUNT(*) FROM `etape` 
WHERE date_etape = '2023-07-12';

# afficher la liste des chauffeurs, avec pour chacun le nombre de tournées qu'il a effectuées

SELECT prenom_chauffeur, nom_chauffeur, COUNT(id_tournee) 
FROM tournee
JOIN chauffeur ON chauffeur.id_chauffeur = tournee.fk_chauffeur
GROUP BY prenom_chauffeur, nom_chauffeur;

# afficher la liste des clients, ordonnée par nombre de bagages transportés

SELECT fk_client, prenom_client, nom_client, SUM(quantite_etape)
FROM etape
JOIN client ON etape.fk_client = client.id_client
GROUP BY fk_client;

# afficher les 10 gîtes qui ont le plus de succès

SELECT nom_gite, COUNT(id_etape) AS nb
FROM gite
JOIN etape ON gite.id_gite = etape.fk_gite_quitter
GROUP BY nom_gite
ORDER BY nb DESC
LIMIT 10;

# afficher le nombre moyen de bagages transpotés pour un client 

SELECT AVG(tot) FROM (
	SELECT prenom_client, nom_client, SUM(quantite_etape) AS tot
	FROM client 
	JOIN etape ON client.id_client = etape.fk_client
	GROUP BY prenom_client, nom_client
) AS qt_clients;

# afficher le camion qui a réalisé le plus de tournées

SELECT immatriculation_camion, COUNT(id_tournee) AS nb_tournees
FROM camion
JOIN tournee ON camion.id_camion = tournee.fk_camion
GROUP BY immatriculation_camion
ORDER BY nb_tournees DESC
LIMIT 1;

# afficher le camion qui a le plus visité le gîte n°10

SELECT immatriculation_camion, fk_gite_rejoindre, COUNT(id_camion) AS nb_tournees
FROM camion
JOIN tournee ON camion.id_camion = tournee.fk_camion
JOIN etape ON tournee.id_tournee = etape.fk_tournee
WHERE etape.fk_gite_rejoindre = 10
GROUP BY immatriculation_camion
ORDER BY nb_tournees DESC
LIMIT 1;

# afficher un planning chauffeur pour un jour donné (liste des gites dans l'ordre de leur index, puis la liste des bagages à récupérer, ainsi que la liste des bagages à déposer)

SELECT id_etape, quantite_etape, fk_gite_quitter, fk_gite_rejoindre
FROM etape
JOIN tournee
ON etape.fk_tournee = tournee.id_tournee
WHERE date_tournee='2023-06-26'
ORDER BY fk_gite_quitter, fk_gite_rejoindre;

# ajouter un nouveau client

INSERT INTO client 
(prenom_client, nom_client, email_client)
VALUES
('Orel', 'Chirot', 'achirot@simplon.co');

# ajoutez trois étapes pour ce client, respectivement pour demain, après-demain et le jour suivant

INSERT INTO etape
(date_etape, fk_client, fk_gite_quitter, fk_gite_rejoindre, quantite_etape)
VALUES
(CURRENT_DATE,10,1,1,5),
(CURRENT_DATE+1,10,1,1,5),
(CURRENT_DATE+2,10,1,1,5);

# ajoutez une tournée pour demain, dont le chauffeur et le camion sont respectivement les derniers de la liste

INSERT INTO tournee
(date_tournee, fk_chauffeur, fk_camion)
SELECT 
	CURRENT_DATE+1,
	MAX(id_chauffeur),
	MAX(id_camion)
FROM chauffeur
JOIN camion;

# Mettez à jour toutes les étapes de demain afin qu'elles soient reliées à la tournée du même jour précédemment créée.

UPDATE etape
SET fk_tournee = 1
WHERE date_etape = CURRENT_DATE+1







DELETE FROM client
WHERE id_client = ANY (
	SELECT id_client
	FROM client
	LEFT JOIN etape ON etape.fk_client = client.id_client
	WHERE id_etape IS NULL
);
    






