<?php 
namespace App;

use \SimpleORM\Model;


class EtapeModel extends Model
{
    //Select the table which the model references
    protected static function getTableName()
    {
        return 'etape';
    }
    //OPTIONAL. Select the id field for the table. Default: 'id'
    protected static function getTableId()
    {
        return 'id_etape';
    }

    //Custom methods for this model

    //public static function myMethod(my_params) { }
}
