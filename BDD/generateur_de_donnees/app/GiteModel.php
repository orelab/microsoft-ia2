<?php 
namespace App;

use \SimpleORM\Model;


class GiteModel extends Model
{
    //Select the table which the model references
    protected static function getTableName()
    {
        return 'gite';
    }
    //OPTIONAL. Select the id field for the table. Default: 'id'
    protected static function getTableId()
    {
        return 'id_gite';
    }

    //Custom methods for this model

    //public static function myMethod(my_params) { }
}
