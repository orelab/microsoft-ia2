<?php 
namespace App;

use \SimpleORM\Model;


class TourneeModel extends Model
{
    //Select the table which the model references
    protected static function getTableName()
    {
        return 'tournee';
    }
    //OPTIONAL. Select the id field for the table. Default: 'id'
    protected static function getTableId()
    {
        return 'id_tournee';
    }

    //Custom methods for this model

    //public static function myMethod(my_params) { }
}
