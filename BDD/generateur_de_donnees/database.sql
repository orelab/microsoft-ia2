DROP TABLE IF EXISTS tournee ;
CREATE TABLE tournee (
	id_tournee BIGINT AUTO_INCREMENT NOT NULL,
	date_tournee DATE,
	fk_camion BIGINT,
	fk_chauffeur BIGINT,
	PRIMARY KEY (id_tournee)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS camion ;
CREATE TABLE camion (
	id_camion BIGINT AUTO_INCREMENT NOT NULL,
	immatriculation_camion TEXT,
	PRIMARY KEY (id_camion)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS client ;
CREATE TABLE client (
	id_client BIGINT AUTO_INCREMENT NOT NULL,
	prenom_client TEXT,
	nom_client TEXT,
	email_client TEXT,
	PRIMARY KEY (id_client)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS gite ;
CREATE TABLE gite (
	id_gite BIGINT AUTO_INCREMENT NOT NULL,
	nom_gite TEXT,
	rue_gite TEXT,
	codepostal_gite TEXT,
	ville_gite TEXT,
	longitude_gite FLOAT,
	latitude_gite FLOAT,
	PRIMARY KEY (id_gite)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS etape ;
CREATE TABLE etape (
	id_etape BIGINT AUTO_INCREMENT NOT NULL,
	date_etape DATE,
	quantite_etape BIGINT,
	fk_gite_quitter BIGINT,
	fk_gite_rejoindre BIGINT,
	fk_tournee BIGINT,
	fk_client BIGINT,
	PRIMARY KEY (id_etape)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS chauffeur ;
CREATE TABLE chauffeur (
	id_chauffeur BIGINT AUTO_INCREMENT NOT NULL,
	prenom_chauffeur TEXT,
	nom_chauffeur TEXT,
	PRIMARY KEY (id_chauffeur)
) ENGINE=InnoDB;

ALTER TABLE tournee ADD CONSTRAINT FK_tournee_camion_id_camion FOREIGN KEY (fk_camion) REFERENCES camion (id_camion);
ALTER TABLE tournee ADD CONSTRAINT FK_tournee_chauffeur_id_chauffeur FOREIGN KEY (fk_chauffeur) REFERENCES chauffeur (id_chauffeur);
ALTER TABLE etape ADD CONSTRAINT FK_etape_id_gite_quitter FOREIGN KEY (fk_gite_quitter) REFERENCES gite (id_gite);
ALTER TABLE etape ADD CONSTRAINT FK_etape_id_gite_rejoindre FOREIGN KEY (fk_gite_rejoindre) REFERENCES gite (id_gite);
ALTER TABLE etape ADD CONSTRAINT FK_etape_id_tournee FOREIGN KEY (fk_tournee) REFERENCES tournee (id_tournee);
ALTER TABLE etape ADD CONSTRAINT FK_etape_id_client FOREIGN KEY (fk_client) REFERENCES client (id_client);
