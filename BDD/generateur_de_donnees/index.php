<?php

require_once 'vendor/autoload.php';

use SimpleORM\Model;
use App\ChauffeurModel;
use App\CamionModel;
use App\TourneeModel;
use App\EtapeModel;
use App\ClientModel;
use App\GiteModel;



SimpleORM\Model::config(array(
    'name' => 'voyageur',
    'user' => 'root',
    'pass' => 'simplon',
    'host' => '127.0.0.1',
    'charset' => 'utf8'
));

$faker = Faker\Factory::create();



// $result = ChauffeurModel::all()->execute();



// Créer 100 gîtes

for($i=0 ; $i<100 ; $i++)
{
	GiteModel::query()->insert([
		"nom_gite" => $faker->word(),
		"rue_gite" => $faker->streetAddress(),
		"codepostal_gite" => $faker->postcode(),
		"ville_gite" => $faker->city(),
		"longitude_gite" => $faker->longitude(),
		"latitude_gite" => $faker->latitude()
	])->execute();
}



// Créer 10 chauffeurs

for($i=0 ; $i<10 ; $i++)
{
	ChauffeurModel::query()->insert([
		"prenom_chauffeur" => $faker->firstName(),
		"nom_chauffeur" => $faker->lastName()
	])->execute();
}



// Créer 10 camions

for($i=0 ; $i<10 ; $i++)
{
	CamionModel::query()->insert([
		"immatriculation_camion" =>
			strtoupper(
				$faker->lexify('??-')  .
				$faker->randomNumber(3,true) .
				$faker->lexify('-??')
			)
	])->execute();
}


// On doit laisser le temps au SGBDR de créer les gîtes avant de poursuivre

sleep(3);



// Créer 50 clients, et entre 3 et 10 étapes par client

for($i=0 ; $i<50 ; $i++)
{
	ClientModel::query()->insert([
		"prenom_client" => $faker->firstName(),
		"nom_client" => $faker->lastName(),
		"email_client" => $faker->email(),
	])->execute();

	$id_client = ClientModel::getLastValue('id_client');
	$id_gite = $faker->numberBetween(1, 85);
	$jour = $faker->dateTimeBetween('now', '+1 year');

	for($j=0 ; $j<50 ; $j++)
	{
		
		EtapeModel::query()->insert([
			"date_etape" => $jour->format('Y-m-d H:i:s'),
			"quantite_etape" => $faker->randomDigitNotNull(),
			"fk_client" => $id_client,
			"fk_gite_quitter" => $id_gite++,
			"fk_gite_rejoindre" => $id_gite
		])->execute();

		$jour->modify('+1 day');
	}

}


