
# Créer l'application de gestion des bagages voyageurs

En tant qu'administrateur, je peux éditer les gîtes, chauffeurs et camions.

En tant que client, je peux me créer un compte utilisateur, et ajouter des étapes.

En tant qu'administrateur, je peux créer des tournées, et affecter les étapes à ces tournées.

En tant qu'administrateur, je peux consulter un graphique du nombre de bagages transportés par jour.

