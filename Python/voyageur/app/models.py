import datetime

from flask_appbuilder import Model

from sqlalchemy import Column, Integer, String, Date, Float, ForeignKey
from sqlalchemy.orm import relationship


class Chauffeur(Model):
    id_chauffeur = Column(Integer, primary_key=True)
    nom_chauffeur = Column(String(100), unique = False, nullable=False)
    prenom_chauffeur = Column(String(100), unique = False, nullable=False)

    def __repr__(self):
        return self.prenom_chauffeur + " " + self.nom_chauffeur

class Camion(Model):
    id_camion = Column(Integer, primary_key=True)
    immatriculation_camion = Column(String(10), unique = True, nullable=False)

    def __repr__(self):
        return self.immatriculation_camion

class Client(Model):
    id_client = Column(Integer, primary_key=True)
    prenom_client = Column(String(100), unique = False, nullable=False)
    nom_client = Column(String(100), unique = False, nullable=False)
    email_client = Column(String(100), unique = False, nullable=False)

    def __repr__(self):
        return self.prenom_client + " " + self.nom_client

class Gite(Model):
    id_gite = Column(Integer, primary_key=True)
    nom_gite = Column(String(10), unique = False, nullable=False)
    rue_gite = Column(String(10), unique = False, nullable=False)
    codepostal_gite = Column(String(10), unique = False, nullable=False)
    ville_gite = Column(String(10), unique = False, nullable=False)
    longitude_gite = Column(Float(), unique = False, nullable=False)
    latitude_gite = Column(Float(), unique = False, nullable=False)

    def __repr__(self):
        return self.nom_gite



class Tournee(Model):
    id_tournee = Column(Integer, primary_key=True)
    date_tournee = Column(Date(), unique = False, nullable=False)
    
    chauffeur_id = Column(Integer, ForeignKey('chauffeur.id_chauffeur'))
    chauffeur = relationship("Chauffeur")
    
    camion_id = Column(Integer, ForeignKey('camion.id_camion'))
    camion = relationship("Camion")

    def __repr__(self):
        return str(self.date_tournee) + " " + self.chauffeur.nom_chauffeur + " " + self.camion.immatriculation_camion


class Etape(Model):
    id_etape = Column(Integer, primary_key=True)
    date_etape = Column(Date, unique = False, nullable=False)
    quantite_etape = Column(Integer, unique = False, nullable=False)
    
    client_id = Column(Integer, ForeignKey('client.id_client'), nullable=False)
    client = relationship("Client")
    
    tournee_id = Column(Integer, ForeignKey('tournee.id_tournee'), nullable=True)
    tournee = relationship("Tournee")

    gite_rejoindre_id = Column(Integer, ForeignKey('gite.id_gite'), nullable=False)
    gite_rejoindre = relationship("Gite", backref='gite_rejoindre', foreign_keys=[gite_rejoindre_id])
    
    gite_quitter_id = Column(Integer, ForeignKey('gite.id_gite'), nullable=False)
    gite_quitter = relationship("Gite", backref='gite_quitter', foreign_keys=[gite_quitter_id])

    # def month_year(self):
    #     return str(self.date_etape.year) +"-" + ("0" + str(self.date_etape.month))[-2:] # ex: "2023-08"

    def month_year(self):
        return datetime.datetime(self.date_etape.year, self.date_etape.month, 1)
    
    def __repr__(self):
        return str(self.date_etape) 
        + " " + self.client.nom_client
        + " (de "+self.gite_quitter.nom + " à " + self.gite_rejoindre.nom+")"
