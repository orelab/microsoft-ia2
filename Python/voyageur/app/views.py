import calendar
from . import appbuilder, db
from flask import render_template
from flask_appbuilder import ModelView, ModelRestApi
from flask_appbuilder.charts.views import DirectByChartView
from flask_appbuilder.models.group import aggregate_count, aggregate_sum, aggregate_avg
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder.security.registerviews import RegisterUserDBView
from flask_appbuilder.widgets import ListBlock, ListItem, ListThumbnail
from flask_babel import lazy_gettext

from .models import Chauffeur, Camion, Gite, Etape, Tournee






class ChauffeurModelView(ModelView):
    datamodel = SQLAInterface(Chauffeur)
    list_columns = ['id_chauffeur', 'nom_chauffeur','prenom_chauffeur']
    list_widget = ListBlock


class CamionModelView(ModelView):
    datamodel = SQLAInterface(Camion)
    list_columns = ['id_camion','immatriculation_camion']
    list_widget = ListBlock


class GiteModelView(ModelView):
    datamodel = SQLAInterface(Gite)
    label_columns = {
        'id_gite':'ID',
        'nom_gite':'Nom',
        'codepostal_gite':'Code postal',
        'ville_gite':'Ville',
    }
    list_columns = ['id_gite', 'nom_gite','codepostal_gite','ville_gite']
    list_widget = ListThumbnail
    show_fieldsets = [
        (
            'Gîte',
            {'fields': ['id_gite','nom_gite']}
        ),
        (
            'Adresse',
            {'fields': ['rue_gite','codepostal_gite','ville_gite'], 'expanded': False}
        ),
        (
            'Localisation',
            {'fields': ['latitude_gite','longitude_gite'], 'expanded': False}
        ),
    ]

class EtapeModelView(ModelView):
    datamodel = SQLAInterface(Etape)
    label_columns = {
        'date_etape':'Date',
        'quantite_etape':'Quantité',
        'client.prenom_client':'Prénom',
        'client.nom_client':'Nom',
        'gite_quitter.nom_gite':'Gîte de départ',
        'gite_rejoindre.nom_gite':'Gîte de destination',
        'month_year()':'mois/annee',
    }
    list_columns = ['date_etape', 'quantite_etape','client.nom_client','gite_quitter.nom_gite','gite_rejoindre.nom_gite','month_year']



class TourneeModelView(ModelView):
    datamodel = SQLAInterface(Tournee)
    label_columns = {
        'date_tournee':'date', 
        'chauffeur.prenom_chauffeur':'prénom', 
        'chauffeur.nom_chauffeur':'nom',
        'camion.immatriculation_camion':'immatriculation',
    }
    list_columns = ['date_tournee', 'chauffeur.prenom_chauffeur', 'chauffeur.nom_chauffeur','camion.immatriculation_camion']



# def pretty_month_year(date):
#     return str(date.year) +"-" + ("0" + str(date.month))[-2:] # ex: "2023-08"
def pretty_month_year(value):
    return calendar.month_name[value.month] + ' ' + str(value.year)

    
class CountryDirectChartView(DirectByChartView):
    '''
    https://flask-appbuilder.readthedocs.io/en/latest/quickcharts.html#grouped-data-charts
    https://github.com/dpgaspar/Flask-AppBuilder/blob/master/flask_appbuilder/charts/views.py
    '''
    datamodel = SQLAInterface(Etape)
    chart_title = 'Charge en bagages du mois en cours'

    definitions = [
    {
        'label': 'Charge de bagages',
        'group': 'month_year',
        'formatter': pretty_month_year,
        'series': [(aggregate_sum,'quantite_etape')],
        # 'series': ['quantite_etape'],
    }
]



class MyRegisterUserDBView(RegisterUserDBView):
    '''
    Customisation enregistrement (ou pas)
    '''
    email_template = 'register_mail.html'
    email_subject = lazy_gettext('Your Account activation')
    activation_template = 'activation.html'
    form_title = lazy_gettext('Fill out the registration form')
    error_message = lazy_gettext('Not possible to register you at the moment, try again later')
    message = lazy_gettext('Registration sent to your email')



appbuilder.add_view(
    ChauffeurModelView,
    "Chauffeurs",
    icon="fa-folder-open-o",
    category="Configuration",
    category_icon='fa-envelope'
)

appbuilder.add_view(
    CamionModelView,
    "Camions",
    icon="fa-folder-open-o",
    category="Configuration",
)

appbuilder.add_view(
    GiteModelView,
    "Gîtes",
    icon="fa-folder-open-o",
    category="Configuration",
)

appbuilder.add_view(
    EtapeModelView,
    "Étapes",
    icon="fa-folder-open-o",
    category="Gestion",
    category_icon='fa-envelope'
)

appbuilder.add_view(
    TourneeModelView,
    "Tournées",
    icon="fa-folder-open-o",
    category="Gestion",
    category_icon='fa-envelope'
)


appbuilder.add_view(
    CountryDirectChartView,
    "Volumes de charge",
    icon="fa-folder-open-o",
    category="Statistiques",
    category_icon='fa-envelope'
)



"""
    Application wide 404 error handler
"""


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        404,
    )


db.create_all()
