
--
-- Déchargement des données de la table `camion`
--

INSERT INTO `camion` (`id_camion`, `immatriculation_camion`) VALUES
(1, 'CN-837-EX'),
(2, 'PX-347-NW'),
(3, 'NS-811-KV'),
(4, 'MX-821-UT'),
(5, 'EY-610-JN'),
(6, 'MW-875-KS'),
(7, 'AF-660-DC'),
(8, 'TZ-280-IL'),
(9, 'MB-478-VV'),
(10, 'VV-215-DM'),
(11, 'RJ-139-KM'),
(12, 'TU-949-TH'),
(13, 'QD-614-GT'),
(14, 'AU-430-AM'),
(15, 'KH-111-QN'),
(16, 'KK-307-WI'),
(17, 'NQ-460-KC'),
(18, 'VZ-177-HL'),
(19, 'XF-381-SA'),
(20, 'SV-813-VZ');
COMMIT;
