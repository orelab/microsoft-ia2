
--
-- Base de données : `voyageur`
--

--
-- Déchargement des données de la table `chauffeur`
--

INSERT INTO `chauffeur` (`id_chauffeur`, `prenom_chauffeur`, `nom_chauffeur`) VALUES
(1, 'Casimir', 'Macejkovic'),
(2, 'Camila', 'Sauer'),
(3, 'Virginie', 'Homenick'),
(4, 'Eileen', 'Smith'),
(5, 'Kianna', 'Cole'),
(6, 'Lyda', 'Barton'),
(7, 'Elna', 'Ondricka'),
(8, 'Brown', 'Jacobi'),
(9, 'Felton', 'Murazik'),
(10, 'Jermaine', 'Wintheiser'),
(11, 'Laurie', 'Robel'),
(12, 'Kenny', 'Beahan'),
(13, 'Alfred', 'Anderson'),
(14, 'Valentin', 'Corkery'),
(15, 'Mireille', 'Walter'),
(16, 'Oliver', 'Hessel'),
(17, 'Valentina', 'Williamson'),
(18, 'Immanuel', 'Fisher'),
(19, 'Gene', 'Langworth'),
(20, 'Roxane', 'Barrows');
COMMIT;
