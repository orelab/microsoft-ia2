
--
-- Base de données : `voyageur`
--

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_client`, `prenom_client`, `nom_client`, `email_client`) VALUES
(2, 'Ryley', 'Treutel', 'ihand@gmail.com'),
(3, 'Natalia', 'Russel', 'esperanza.christiansen@hotmail.com'),
(4, 'Percy', 'Willms', 'lebsack.juliana@yahoo.com'),
(5, 'Ebba', 'Littel', 'giovanna.wolff@thiel.org'),
(6, 'Kiel', 'Parisian', 'stiedemann.zakary@yahoo.com'),
(7, 'Josiah', 'McGlynn', 'moses.gorczany@yahoo.com'),
(8, 'Stacy', 'Jacobs', 'cecilia65@yahoo.com'),
(9, 'Yasmine', 'King', 'jast.aaron@gmail.com'),
(10, 'Theo', 'Carter', 'kaleigh43@gmail.com'),
(11, 'Maribel', 'Boyer', 'volkman.philip@hotmail.com'),
(12, 'Rahul', 'Jacobs', 'rrowe@gmail.com'),
(13, 'Justice', 'Wiza', 'skling@gmail.com'),
(14, 'Gardner', 'Douglas', 'watsica.toney@hotmail.com'),
(15, 'Torrance', 'Stark', 'heaney.roderick@yahoo.com'),
(16, 'Jaclyn', 'McDermott', 'josefa39@kohler.com'),
(17, 'Alana', 'Johns', 'iva06@hotmail.com'),
(18, 'Jacynthe', 'Kuphal', 'collier.pamela@yahoo.com'),
(19, 'Cordia', 'Murazik', 'collier.lennie@yahoo.com'),
(20, 'Virgie', "O'Hara", 'wolf.nia@borer.com'),
(21, 'Otis', 'Beer', 'cyrus.koss@gmail.com'),
(22, 'Alana', 'Weissnat', 'russel.consuelo@braun.com'),
(23, 'Parker', 'Little', 'xlemke@yahoo.com'),
(24, 'Trevion', 'Dickinson', 'reichel.nicolas@abernathy.com'),
(25, 'Rowena', 'Raynor', 'ospinka@rodriguez.com'),
(26, 'Tanya', 'Bauch', 'brennon.emard@gmail.com'),
(27, 'Cooper', 'Wunsch', 'norene.skiles@yahoo.com'),
(28, 'Rozella', 'Vandervort', 'kunze.lilliana@wiegand.info'),
(29, 'Fausto', 'Orn', 'berge.berta@moore.com'),
(30, 'Julian', 'Brakus', 'blanda.maymie@corwin.com'),
(31, 'Myrtie', 'Kohler', 'emmerich.yasmeen@hotmail.com'),
(32, 'Tony', 'Feest', 'vernon26@trantow.net'),
(33, 'Gus', 'Price', 'shyanne22@gmail.com'),
(34, 'Heather', 'Windler', 'lauriane.moen@yahoo.com'),
(35, 'Aaron', 'Zieme', 'jessie29@hegmann.com'),
(36, 'Dejah', 'Ondricka', 'jarrett21@yahoo.com'),
(37, 'Malika', 'Dibbert', 'alanis91@hotmail.com'),
(38, 'Zetta', 'Senger', 'domenico.schoen@feil.net'),
(39, 'Willy', 'Gerhold', 'jane.dooley@stiedemann.biz'),
(40, 'Green', 'Steuber', 'hturner@funk.com'),
(41, 'Garland', 'Ankunding', 'olin80@jacobs.org'),
(42, 'Lazaro', 'Bernier', 'brakus.donald@yahoo.com'),
(44, 'Loyce', 'Zboncak', 'romaguera.roxane@gmail.com'),
(45, 'Vergie', 'Schroeder', 'beth56@yahoo.com'),
(46, 'Ariel', 'Hessel', 'ucruickshank@yahoo.com'),
(47, 'Eldon', 'Wisoky', 'bart.hoeger@gmail.com'),
(48, 'Josephine', 'Hessel', 'grady.dayna@bechtelar.net'),
(49, 'Velma', 'Gutkowski', 'bfahey@leffler.com'),
(50, 'Fernando', 'Glover', 'vjacobs@pagac.org'),
(51, 'Dolly', 'Schuster', 'kris.louvenia@smitham.com'),
(52, 'Hattie', 'Konopelski', 'dortha.block@metz.info'),
(53, 'Armani', 'Kihn', 'christiana50@hotmail.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
