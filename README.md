

# Projets

## Bases de données

Vous retrouverez dans le dossier les briefs sur la modélisation 
et le SQL, les requêtes SQL que j'avais faites avec vous, ainsi
que les schéma MCD et le fichier .asi à ouvrir avec le logiciel
gratuit AnalyseSI.

Vous trouverez aussi l'image Docker de MariaDB + PhpMyAdmin
(celle qui nous a posé quelques problèmes !). Il se peut
que j'ai trouvé l'erreur, et elle devrait tourner aussi bien
sous n'importe quel OS maintenant (vous me direz !).

En bonus, vous avez le code PHP que j'ai écrit pour générer des 
données "fakes" pour peupler vos bases de données ;)


## Python

Vous avez ici le brief "Voyageur" qui reprend la dernière base
de données modélisée la semaine précédente, ainsi que le source
du projet que j'ai réalisé en même temps que vous.

Notez que la base de données est un fichier SQLite, et qu'il est
versionné. Ce n'est pas une chose à faire en général, mais dans
notre cas, ça simplifie, et c'est un projet démo.

- la base de données ets préchargée avec des données test
- le compte 'admin' a comme mot de passe 'simplon'
- un compte utilisateur 'michel', avec mot de passe 'michel'


#### Installation :

```
git clone git@gitlab.com:orelab/microsoft-ia2.git
cd microsoft-ia2/Python/voyageur/
python3 -m venv .venv
mv config.py.tpl config.py
```

#### Démarrage :
```
./start.sh
```

