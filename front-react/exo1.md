# CV automatique

Créez un formulaire d'inscription multi-écrans pour générer un CV :

- premier écran : mes informations personnelles
- second écran : mes titres et diplômes
- troisième écran : mes expériences professionnelles
- quatrième écran : compétences transverses, centres d'intérêt, divers
- cinquième écran : affichage de mon CV !
  (en fait, un résumé des valeurs collectées)

