# Je fais mes courses

Créer une application qui affiche sur une colonne de gauche une liste de produits
(titre, texte, prix, illustration, couleur, taille).
Chaque produit est doté d'un bouton de mise au panier.

Sur la colonne de droite, vous affichez le panier, avec le montant total.

## améliorations

Ajoutez un système de filtre au dessus des produits, pour faire une restriction par 
couleur et/ou taille.

Ajoutez la possibilité de modifier les quantités dans le panier.

## Ajouter un backend

Strapi est un CMS dit "headless". C'est une base de données couplée à une interface
d'administration, ainsi qu'à une API !

Installez Strapi, configurez une table "produits" et une table "paniers"
(et toutes les autres tables dont vous pourriez avoir besoin).

Modifiez votre application React pour que les produits affichés soient tirés de Strapi
(utilisez la fonction javascript "fetch"), et ajoutez une fonction d'enregistrement du 
panier vers Strapi.

Pour cet exercice, ne vous embarassez pas d'authentification.