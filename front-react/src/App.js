import {useState} from 'react';

import './App.css';
import Card from './Card';
import CV from './CV';
import avatar from './avatar.png';


function App() {

  const [page, setPage] = useState(0);
  const [data, setData] = useState([]);

  const moveNext = () => setPage(page<4 ? page+1 : page);
  const movePrev = () => setPage(page>0 ? page-1 : page);

  return (
    <div className="App">
      <h1>Générateur de CV</h1>

      <Card 
        img={avatar} color={"rgba(255,200,100,.3)"} 
        visibility={page===0}
        moveNext={moveNext} movePrev={movePrev} 
        data={data} setData={setData}>

        <h2>Mes informations personnelles</h2>
        <label for="Prénom">Prénom</label>
        <input type="text" name="Prénom" />
        <br/>
        <label for="Nom">Nom</label>
        <input type="text" name="Nom" />
        <br/>
      </Card>

      <Card 
        img={avatar} color={"rgba(100,200,0,.3)"} 
        visibility={page===1}
        moveNext={moveNext} movePrev={movePrev} 
        data={data} setData={setData}>

        <h2>Titre ou diplôme le plus élevé</h2>
        <label for="Titre">Titre</label>
        <input type="text" name="Titre" />
        <br/>
        <label for="Année">Année d'obtention</label>
        <input type="text" name="Année" />
        <br/>
      </Card>
    
      <Card 
        img={avatar} color={"rgba(100,0,100,.3)"} 
        visibility={page===2}
        moveNext={moveNext} movePrev={movePrev} 
        data={data} setData={setData}>

        <h2>Expérience professionnelle</h2>
        <label for="Job[0]">Poste</label>
        <input type="text" name="Job[0]" />
        <br/>
        <label for="Entreprise[0]">Entreprise</label>
        <input type="text" name="Entreprise[0]" />
        <br/>
        <label for="Année entreprise[0]">Année</label>
        <input type="text" name="Année entreprise[0]" />
        <br/>
        <br/>

        <label for="job[1]">Poste</label>
        <input type="text" name="job[1]" />
        <br/>
        <label for="Entreprise[1]">Entreprise</label>
        <input type="text" name="Entreprise[1]" />
        <br/>
        <label for="Année entreprise[1]">Année</label>
        <input type="text" name="Année entreprise[1]" />
        <br/>
      </Card>

      <Card 
        img={avatar} color={"rgba(100,200,100,.3)"} 
        visibility={page===3}
        moveNext={moveNext} movePrev={movePrev} 
        data={data} setData={setData}>

        <h2>Divers</h2>
        <label for="Comp. diverses">Autres compétences</label>
        <textarea name="Comp. diverses" row="4"></textarea>
        <br/>
        <label for="Centres d'intérêt">Centres d'intérêts</label>
        <textarea name="Centres d'intérêt" row="4"></textarea>
        <br/>

        <label for="Divers">Infos complémentaires</label>
        <textarea name="Divers" row="4"></textarea>
        <br/>
      </Card>

      <CV data={data} movePrev={movePrev} visibility={page===4} />

    </div>
  );
}

export default App;
