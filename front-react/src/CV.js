
export default function CV({visibility, data, movePrev}){

  return (
    <div style={{display:(visibility?'block':'none')}}>
      <table>
        {Object.keys(data).map(key => 
          <tr>
            <th>{key}</th>
            <td>{data[key]}</td>
          </tr>
        )}
      </table>
      <a href="none" onClick={e => {e.preventDefault(); movePrev()}}>retour</a>
    </div>
  );
}