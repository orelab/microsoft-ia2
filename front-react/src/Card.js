import './Card.css';

function Card({children, color, visibility, moveNext, movePrev, data, setData}){

  const doSave = ev => {
      setData({...data, [ev.target.name]:ev.target.value})
  }

  return (
    <div class="card" style={{display:(visibility?'block':'none'),backgroundColor:color}}>
      <form onChange={doSave}>
        {children}
      </form>
      <button onClick={movePrev}>précédent</button>
      <button onClick={moveNext}>suivant</button>
    </div>
  );
}

export default Card;